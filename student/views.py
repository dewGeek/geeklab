from django.shortcuts import render
import urllib
from django.shortcuts import render_to_response, redirect, render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template.context import RequestContext
from django.core.context_processors import csrf
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from forms import *
from models import *
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib import auth
import json


def home(request):
    if request.user.is_authenticated():
        username = request.user.username
        
        return HttpResponseRedirect("/student/%s/profile"  % urllib.quote(username) )

    
    return render(request, 'home.html')



def register(request):

    context = RequestContext(request)

    if request.method =='POST':

        form = UserForm(request.POST)
        if form.is_valid():

            username=form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = User.objects.create_user(username=username, password=password )
            user.save()

            #authenicate user after registeration
            current_user = authenticate(username=username, password=password)

            if current_user is not None:

                auth.login(request, current_user)
            
                return HttpResponseRedirect("/student/%s/profile" % urllib.quote(username) )
        else:
            print form.errors

            return render(request, 'home.html', context_instance=context)


def login(request):
    

    context = RequestContext(request)

    if request.method == 'POST':
        
        username = request.POST['username']
        password = request.POST['password']

        student = authenticate(username=username, password=password)
        if student is not None:
            auth.login(request, student)
            
            return HttpResponseRedirect("/student/%s/profile" % urllib.quote(username) )

        else:

            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")

            #return HttpResponseRedirect(reverse(invalid_login))
    else:
        
        return render_to_response('home.html', context)



@login_required
def profile(request, username):

    user = get_object_or_404(User, username=username) #Calls get() on a given model manager
    student = StudentProfile.objects.get(user=user)
    data = {}

    print user

    try: 
        StudentProfile.objects.filter(user = request.user).exists()

        profile =  StudentProfile.objects.get(user = request.user)

        data['name'] = profile.name
        data['website'] = profile.website

    except (StudentProfile.DoesNotExist) :

        data['name'] = "your full name here"
        data['website'] = "add your blog/website" 



    try:
        #student = StudentProfile.objects.get(user=user)
        skill = Skills.objects.filter(student= student)
        data['skills'] = skill

    except (Skills.DoesNotExist):
        data['title'] = ''


    try:
        #student = StudentProfile.objects.get(user=user)
        exp = Experience.objects.filter(student= student)
        data['exp'] = exp

    except (Experience.DoesNotExist):
        print "does not exists"


    return render_to_response( 'dashboard.html', {'info': data } ,context_instance=RequestContext(request))



def logout(request):
    auth.logout(request)
    return render_to_response('home.html')

@login_required
def invalid_login(request):

   return render_to_response('invalid_login.html')


@login_required
def hackathons(request):

    return render_to_response('hackathons.html')


@login_required
def edit(request,username):

    context = RequestContext(request)

    user = User.objects.get(username=request.user)
    student = StudentProfile.objects.get(user=user)

    data ={}

    try:
        
        profile = StudentProfile.objects.filter(user = request.user)

        data['profile'] = profile
        for i in profile:
            data['name'] = i.name
            data['website'] = i.website
        
    except (StudentProfile.DoesNotExist) :
        
        data['name'] = "your full name here"
        data['website'] = "add your blog/website" 

    try: 
        #student = StudentProfile.objects.get(user= user)   
        person_skills = Skills.objects.filter(student= student)
        data['skill'] = person_skills

    except (Skills.DoesNotExist):
        data['title'] = ''


    try:
        #student = StudentProfile.objects.get(user= user) 
        exp = Experience.objects.filter(student= student)
        data['exp'] = exp
        
    except (Experience.DoesNotExist):
        print "does not exists"


    return render_to_response('edit.html', {'info': data}, context)


@login_required
def editbio(request):

    try:
        pid = request.POST['id']

    except (KeyError):
        pid = ""

    if pid:
        instance_id = get_object_or_404(StudentProfile, pk= pid)

    else:
        instance_id = StudentProfile(user= request.user)

    if request.method == 'POST':
       
        basic_form = StudentProfileForm(request.POST, instance = instance_id)    

        if basic_form.is_valid():

            info = basic_form.save(commit=False)
            
            info.save()

            response_dict = {} 
            y = "success"

            response_dict.update({'server_response': y })  

            return HttpResponse(json.dumps(response_dict))
        else:
            
            print basic_form.errors

            response_dict = {} 

            response_dict.update({'found an error': 'found an error' })  

            return HttpResponse(json.dumps(response_dict))

@login_required
def addskills(request):

    if request.method == 'POST':

        skill_form = SkillsForm(request.POST)
        if skill_form.is_valid():

            logged_user = StudentProfile.objects.get(user= request.user)

            skills = skill_form.save(commit=False)
            skills.student = logged_user
            skills.save()


            response_dict = {} 
            y = "success"

            response_dict.update({'result': y })  

            return HttpResponse(json.dumps(response_dict))
        else:
            
            print skill_form.errors

            response_dict = {} 

            response_dict.update({'found an error': 'found an error' })  

            return HttpResponse(json.dumps(response_dict))


@login_required
def addexp(request):

    if request.method == 'POST':
        
        exp_form = ExperienceForm(request.POST)
        if exp_form.is_valid():

            logged_user = StudentProfile.objects.get(user= request.user)
            
            exp = exp_form.save(commit=False)
            exp.student = logged_user
            exp.save()

            response_dict = {} 
            y = "success"

            response_dict.update({'result': y })  

            return HttpResponse(json.dumps(response_dict))
        else:
            
            print exp_form.errors

            response_dict = {} 

            response_dict.update({'found an error': 'found an error' })  

            return HttpResponse(json.dumps(response_dict))
