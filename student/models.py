
from django.db import models
from django.contrib.auth.models import User
from taggit.managers import TaggableManager


class StudentProfile(models.Model):
	
	user = models.ForeignKey(User, unique=True)
	#chapter = models.CharField(max_length=20)
	name = models.CharField(max_length=30)
	website = models.URLField(max_length=50)
	

	def __unicode__(self):
		return self.user.username


#Student.profile = property(lambda u: Student.objects.get_or_create(user=u)[0])


class Skills(models.Model):

	student = models.ForeignKey(StudentProfile)
	title = models.CharField(max_length=50)

	def __unicode__(self):
		return self.title

class Experience(models.Model):

	student = models.ForeignKey(StudentProfile)
	company = models.CharField(max_length=25)
	profile = models.CharField(max_length=25)
	location = models.CharField(max_length=20)
	duration = models.CharField(max_length=20)

	def __unicode__(self):
		return self.profile
