from django.contrib import admin
from student.models import StudentProfile, Skills, Experience

admin.site.register(StudentProfile)
admin.site.register(Skills)
admin.site.register(Experience)



