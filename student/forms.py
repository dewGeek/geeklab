
from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from models import *


class UserForm(forms.ModelForm):

	class Meta:
		model = User
		fields = ('username', 'password')

	def clean_username(self):
		username = self.cleaned_data['username']

		try:
			User.objects.get(username = username)
			raise forms.ValidationError("That username is already taken"+username)

		except User.DoesNotExist:

			return username

	def clean_password(self):
		password = self.cleaned_data.get('password', '')
		#pswd1 = self.cleaned_data['password']
		if password == '':
			raise forms.ValidationError("Password should be atleast 2 characters ")
		"""if pswd != pswd1:
			raise forms.ValidationError("Passwords do not match") """
		return password	
	
	"""def clean_email(self):
		email_id = self.cleaned_data["email"]
		try:
			user_id = User.objects.get(email = email_id)
			raise forms.ValidationError("That email is already taken")
		except User.DoesNotExist:
			return email_id """



class StudentProfileForm(forms.ModelForm):

	class Meta:
		model = StudentProfile
		fields = ('name', 'website')


class SkillsForm(forms.ModelForm):

	class Meta:
		model = Skills
		fields = ('title',)


class ExperienceForm(forms.ModelForm):

	class Meta:
		model = Experience
		fields = ('company', 'profile', 'location', 'duration')



