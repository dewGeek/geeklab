from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    #url(r'^accounts/', include('allauth.urls')),
    url(r'^', include('student.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^student/', include('student.urls')),
    #url(r'^corporate/', include('corporate.urls')),
    #url(r'^academic/', include('acamedic.urls')),


)


